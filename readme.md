# Setup

1. Requires Sql Server management studio. Download the [Express Version](https://www.microsoft.com/en-gb/sql-server/sql-server-downloads) for free.

2. Connect to local database
   - Server name: `.` (yes that is a dot, just a dot)
   - Authentication: `Windows Authentication`
   - Connect

![login dialog](./images/login.png)

3. Open and run files 1 and 2 from the scripts folder in order. This will build and populate the database with data

# Resources

- A resource that is probably way better than this demo can be found at [W3Schools](https://www.w3schools.com/sql/default.asp)

- If in doubt, remember Google is your friend

![developers](./images/developers.png)

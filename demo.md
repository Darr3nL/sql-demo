# SQL Demo

## What is SQL?

- Structured Query Language
    - It sits on top of a database and allows us to query it in a readable way
- Used for storing, manipulating and retrieving data stored in a relational database.
- SQL is a Standard - BUT...
    - Although SQL is an ANSI/ISO standard, there are different versions of the SQL language.
    - Sorted use MSSQL and thats what this demo will cover

## Uses of Databases

- Data warehousing
- Normalization
- Backends to web sites / services
- Report Generation

## What Can SQL do?

- Allows users to access data in the relational database management systems.
- Allows users to describe the data.
- Allows users to define the data in a database and manipulate that data.
- Allows users to create and drop databases and tables.
- Allows users to create view, stored procedure, functions in a database.
- Allows users to set permissions on tables, procedures and views.

## Concepts

- A server can contain many databases
- A database is a collection of tables
- Tables are a collection of records
    - A record, also called a row, is each individual entry that exists in a table. A record is a horizontal entity in a table.
    - A column is a vertical entity in a table that contains all information associated with a specific field in a table.
- Referential integrity
- Mathematical and summary operations are available
- Good at combining data from mutliple tables

## Commands

- `CREATE` - Creates a new table, a view of a table, or other object in the database
- `ALTER` - Modifies an existing database object, such as a table
- `DROP` - Deletes an entire table, a view of a table or other objects in the database
- `SELECT` - Retrieves certain records from one or more tables.
- `INSERT` - Creates a record.
- `UPDATE` - Modifies records.
- `DELETE` - Deletes records.


## Examples

1) [SELECT statements](./examples/01-select.md)
2) [CREATE, ALTER and DROP](./examples/02-create-alter-drop.md)
3) [INSERT, UPDATE, and DELETE](./examples/03-insert-update-delete.md)
4) [Foreign Keys](./examples/05-join-tables.md)
5) [Joins](./examples/06-more-joins.md)


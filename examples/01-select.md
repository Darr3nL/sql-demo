# Select

You can select an entire table by using the `*` wildcard

```SQL
SELECT *
FROM Employees
```

| Id  | Forename | Surname | Department |
| --- | :------- | :------ | :--------- |
| 1   | Jordan   | Harper  | Dev        |
| 2   | Toby     | Young   | Dev        |
| 3   | Alfie    | Wong    | Dev        |
| 4   | Imogen   | Pearson | Dev        |

_Note that this is generally considered bad practice and you should ideally specify the column names to avoid any breaking changes should the table be changed as shown in the next examples._

# What is a record

A record is a single row from within a table, you can (and probably almost always should) select specific rows by using clauses such as `WHERE`

```SQL
SELECT
	Id,
	Forename,
	Surname,
	Department
FROM Employees
WHERE Forename = 'Jordan'
  AND Surname = 'Harper'
```

| Id  | Forename | Surname | Department |
| --- | :------- | :------ | :--------- |
| 1   | Jordan   | Harper  | Dev        |

## What is a column

Selecting a row will give you a single property for the entities in that table. You can select a single column from a a database by specifying the column you want.

```SQL
SELECT Forename
FROM Employees
```

| Forename |
| :------- |
| Jordan   |
| Toby     |
| Alfie    |
| Imogen   |

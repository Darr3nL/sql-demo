# CREATE, ALTER and DROP

## CREATE

`CREATE` is the command used to create new objects in the database such as Tables, Functions, Stored Procedures and a few other things that we wont get into in this demo.

In this example we are going to create a new table to contain information for the employees departments we can run the following

```SQL
CREATE TABLE Departments (
	[Id]		  INT IDENTITY(1,1) PRIMARY KEY,
	[Description] NVARCHAR(100) NOT NULL
)

INSERT INTO Departments([Description])
VALUES
  ('Dev'),
  ('QA'),
  ('Support'),
  ('BA')

SELECT * FROM Departments
```

| **Id** | **Description** |
| :----: | :-------------: |
|   1    |       Dev       |
|   2    |       QA        |
|   3    |     Support     |
|   4    |       BA        |

# ALTER

`ALTER` is used to make changes to various entities in a database. In the following example we use the `ALTER TABLE` command to add and then remove a column to an existing table.

To add a numeric column which would contain the CostPerHour value of each Departments we could do the following

```SQL
ALTER TABLE Departments
ADD CostPerHour INT

SELECT TOP 1 * FROM Departments
```

| **Id** | **Description** | **CostPerHour** |
| :----: | :-------------: | :-------------: |
|   1    |       Dev       |      NULL       |
|   2    |       QA        |      NULL       |
|   3    |     Support     |      NULL       |
|   4    |       BA        |      NULL       |

To remove a column from a table you can use the `DROP COLUMN` command

```SQL
ALTER TABLE Departments
DROP COLUMN CostPerHour
```

## DROP

The `DROP` command is used to remove a database or table from a database

```SQL
DROP TABLE Employees
```

_Be careful before dropping a table. Deleting a table will result in loss of complete information stored in the table!_

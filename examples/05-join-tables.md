# Join

One table on its own isnt often very useful. SQL allows us to join two or more tables on using related information between them. In this example we'll be using the deparment foreign key we added to the Employees table.

```SQL
SELECT *
FROM Employees
JOIN Departments ON Employees.DepartmentId = Departments.Id
```

| **Id** | **Forename** | **Surname** | **DepartmentId** | **Id** | **Description** | **CostPerHour** |
| :----: | :----------: | :---------: | :--------------: | :----: | :-------------: | :-------------: |
|   5    |   Jennifer   |    Pratt    |        1         |   1    |       Dev       |       20        |
|   6    |    Louie     |    Black    |        2         |   2    |       QA        |       15        |
|   7    |  Alexandra   |  Richards   |        2         |   2    |       QA        |       15        |
|   9    |    Amelie    |   Davison   |        3         |   3    |     Support     |       13        |

## Aliases

As you can see we have some columns which are named the same. We can use Aliases to make the data easier to read

```SQL
SELECT
 Employees.Id AS EmployeeId,
 Employees.Forename + ' ' + Employees.Surname AS Employee,
 Departments.Description AS DepartmentName,
 Departments.CostPerHour
FROM Employees
JOIN Departments ON Employees.DepartmentId = Departments.Id
```

| **EmployeeId** |    **Employee**    | **DepartmentName** | **CostPerHour** |
| :------------: | :----------------: | :----------------: | :-------------: |
|       5        |   Jennifer Pratt   |        Dev         |       20        |
|       6        |    Louie Black     |         QA         |       15        |
|       7        | Alexandra Richards |         QA         |       15        |
|       9        |   Amelie Davison   |      Support       |       13        |

As this is quite verbose you can also give the table names Aliases so the SQL becomes

```SQL
SELECT
 e.Id AS EmployeeId,
 e.Forename + ' ' + e.Surname AS Employee,
 d.Description AS DepartmentName,
 d.CostPerHour
FROM Employees   e
JOIN Departments d ON e.DepartmentId = d.Id
```

# Even more joins

In this example we will calculate the total cost of all time spent on a specific work item.

## View all workitems

The following query gets us some information about the work items

```SQL
SELECT
  p.Reference As ProductRef,
  w.Reference As WorkItemRef,
  w.Title,
  r.Reference As CarrierReference,
  c.Reference As ClientReference
FROM Products       p
JOIN WorkItems      w ON p.Id = w.ProductId
LEFT JOIN Carriers  r ON w.CarrierId = r.Id
LEFT JOIN Clients   c ON c.Id = w.ClientId
```

| **ProductRef** | **WorkItemRef** |        **Title**         | **CarrierReference** | **ClientReference** |
| :------------: | :-------------: | :----------------------: | :------------------: | :-----------------: |
|     React      |      42154      |    React integration     |         NULL         |     NBrownGroup     |
|   SortedPro    |      31548      |  SortedPRO integration   |         NULL         |   GrahamAndBrown    |
|   SortedPro    |      52923      | DPD routing data import  |         DPD          |      FarFetch       |
|   SortedPro    |      53023      | Lineten booking service  |       Lineten        |      FarFetch       |
|   SortedPro    |      53015      | Lineten collection notes |       Lineten        |      FarFetch       |
|      MPD       |      12364      |  New feature for Yodel   |        Yodel         |        NULL         |

## Get all activies for a specific work item

If we were only interested in the activies logged against 53015 - Lineten collection notes we could perform the following query

```SQL
SELECT
  w.Reference,
  w.Title,
  e.Forename + ' ' + e.Surname AS Employee,
  a.ActivityType,
  a.Date,
  a.StartTime,
  a.EndTime
FROM Activities      a
JOIN WorkItems       w ON w.Id = a.WorkItemId
JOIN Clients         c ON c.Id = w.ClientId
JOIN Employees       e ON e.Id = a.EmployeeId
WHERE w.Reference = 53015
ORDER BY a.Date, a.StartTime
```

| **Reference** |        **Title**         |  **Employee**  | **ActivityType** |  **Date**  | **StartTime** | **EndTime** |
| :-----------: | :----------------------: | :------------: | :--------------: | :--------: | :-----------: | :---------: |
|     53015     | Lineten collection notes |   Toby Young   |     Analysis     | 2019-05-14 |     09:00     |    12:00    |
|     53015     | Lineten collection notes | Matthew Warner |     Analysis     | 2019-05-14 |     09:00     |    12:00    |
|     53015     | Lineten collection notes |   Alfie Wong   |   Development    | 2019-05-16 |     09:00     |    12:00    |
|     53015     | Lineten collection notes |   Alfie Wong   |   Development    | 2019-05-16 |     09:00     |    12:00    |
|     53015     | Lineten collection notes |   Alfie Wong   |   Development    | 2019-05-17 |     09:00     |    12:00    |
|     53015     | Lineten collection notes |   Alfie Wong   |   Development    | 2019-05-17 |     09:00     |    12:00    |
|     53015     | Lineten collection notes |   Alfie Wong   |   Development    | 2019-05-18 |     09:00     |    12:00    |
|     53015     | Lineten collection notes |   Alfie Wong   |   Development    | 2019-05-19 |     09:00     |    12:00    |
|     53015     | Lineten collection notes |  Louie Black   |     Testing      | 2019-05-19 |     09:00     |    12:00    |
|     53015     | Lineten collection notes |  Louie Black   |     Testing      | 2019-05-19 |     09:00     |    12:00    |
|     53015     | Lineten collection notes |  Louie Black   |     Testing      | 2019-05-19 |     09:00     |    12:00    |

However, unless you're a robot 🤖 this doesnt feel particularly useful.

## Work out how much it all cost

In this example we're not interested in who did the work or what day it was, but we are interested in how much money was spent at each stage of the work items life cycle.

The following query is a little more complicated and uses [SUM](https://www.w3schools.com/sql/sql_count_avg_sum.asp) and [GROUP BY](https://www.w3schools.com/sql/sql_groupby.asp) statements but returns some more useful information

```SQL
SELECT
  w.Reference,
  w.Title,
  a.ActivityType,
  SUM(DATEDIFF(hour, a.StartTime, a.EndTime)) AS TimeSpent,
  SUM(d.CostPerHour * DATEDIFF(hour, a.StartTime, a.EndTime)) AS TotalCost
FROM Activities      a
JOIN WorkItems       w ON w.Id = a.WorkItemId
JOIN Clients         c ON c.Id = w.ClientId
JOIN Employees       e ON e.Id = a.EmployeeId
JOIN Departments     d ON d.Id = e.DepartmentId
WHERE w.Reference = 53015
GROUP BY w.Reference, Title, ActivityType
```

| **Reference** |        **Title**         | **ActivityType** | **TimeSpent** | **TotalCost** |
| :-----------: | :----------------------: | :--------------: | :-----------: | :-----------: |
|     53015     | Lineten collection notes |     Analysis     |       6       |      111      |
|     53015     | Lineten collection notes |   Development    |      49       |      980      |
|     53015     | Lineten collection notes |     Testing      |      21       |      315      |

### Alternatively

If we wanted to see this breakdown for all work items for the Lineten carrier we could simply change our `WHERE` clause to filter by the CarrierId rather than the WorkItem Reference

```SQL
SELECT
  w.Reference,
  w.Title,
  a.ActivityType,
  SUM(DATEDIFF(hour, a.StartTime, a.EndTime)) AS TimeSpent,
  SUM(d.CostPerHour * DATEDIFF(hour, a.StartTime, a.EndTime)) AS TotalCost
FROM Activities      a
JOIN WorkItems       w ON w.Id = a.WorkItemId
JOIN Clients         c ON c.Id = w.ClientId
JOIN Employees       e ON e.Id = a.EmployeeId
JOIN Departments     d ON d.Id = e.DepartmentId
WHERE w.CarrierId = 5
GROUP BY w.Reference, Title, ActivityType
```

| **Reference** |        **Title**         | **ActivityType** | **TimeSpent** | **TotalCost** |
| :-----------: | :----------------------: | :--------------: | :-----------: | :-----------: |
|     53015     | Lineten collection notes |     Analysis     |       6       |      111      |
|     53015     | Lineten collection notes |   Development    |      49       |      980      |
|     53015     | Lineten collection notes |     Testing      |      21       |      315      |
|     53023     | Lineten booking service  |     Analysis     |       9       |      171      |
|     53023     | Lineten booking service  |   Development    |      27       |      540      |
|     53023     | Lineten booking service  |     Testing      |       9       |      135      |

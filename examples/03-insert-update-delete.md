# Insert, Update and Delete

## Insert

`INSERT` is the command used to add data to a table in the following example you will see that there is no Employee record with the Id of 13 until we add one using the `INSERT` statement.

```SQL
INSERT INTO Departments (Description, CostPerHour)
VALUES('Management', 50)

SELECT * FROM Departments
```

| **Id** | **Description** | **CostPerHour** |
| :----: | :-------------: | :-------------: |
|   1    |       Dev       |      NULL       |
|   2    |       QA        |      NULL       |
|   3    |     Support     |      NULL       |
|   4    |       BA        |      NULL       |
|   5    |   Management    |       50        |

## Update

`UPDATE` is the command used to update existing data in the database. We specify which columns we want to update and the values we wish to apply. Also, we can (and probably should) specify and filter to limit the rows we wants to update using a `WHERE` clause.

```SQL
UPDATE Departments
SET CostPerHour = 20
WHERE Description = 'Dev'

SELECT * FROM Departments
```

| **Id** | **Description** | **CostPerHour** |
| :----: | :-------------: | :-------------: |
|   1    |       Dev       |       20        |
|   2    |       QA        |      NULL       |
|   3    |     Support     |      NULL       |
|   4    |       BA        |      NULL       |
|   5    |   Management    |       50        |

The following is a more advanced statement which will update the remainig values for us rather than doing them one at a time

```SQL
UPDATE Departments
SET CostPerHour = CASE Description
				  WHEN 'QA' THEN 15
				  WHEN 'Support' THEN 13
				  WHEN 'BA' THEN 17
				  END
WHERE CostPerHour IS NULL
```

| **Id** | **Description** | **CostPerHour** |
| :----: | :-------------: | :-------------: |
|   1    |       Dev       |       20        |
|   2    |       QA        |       15        |
|   3    |     Support     |       13        |
|   4    |       BA        |       17        |
|   5    |   Management    |       50        |

## Delete

`DELETE` is another pretty self explanatory statement which removes data from a table.

For example the following statement would delete the record with the Id of 5.

```SQL
DELETE FROM Departments WHERE Id = 5
```

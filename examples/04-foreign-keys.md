# Primary and Foreign Keys

So far each table in our database contains at least one `Id` column. This is known as a Primary Key and is a constraint which uniquely identifies each record in a table.

A foreign key is a key used to link two tables together and is a field in one table that refers to the PRIMARY KEY in another table.

Now that we have a new table for our departments we need to join the Employees and the Departments table together using their keys.

## Add a column

The first thing we need to do is add the a column to Employees which is going to reference the Departments table

```SQL
ALTER TABLE Employees
ADD DepartmentId INT
FOREIGN KEY(DepartmentId) REFERENCES Departments(id)
```

| **Id** | **Forename** | **Surname** | **Department** | **DepartmentId** |
| :----: | :----------: | :---------: | :------------: | :--------------: |
|   5    |   Jennifer   |    Pratt    |      Dev       |       NULL       |
|   6    |    Louie     |    Black    |       QA       |       NULL       |
|   7    |  Alexandra   |  Richards   |       QA       |       NULL       |
|   9    |    Amelie    |   Davison   |    Support     |       NULL       |

## Set the values

Next, we need to update the table to set the values using the Description.

You might notice a new bit of syntax here and that's called a [subquery](https://docs.microsoft.com/en-us/sql/relational-databases/performance/subqueries?view=sql-server-ver15). Going into detail about this syntax is probably beyond the scope of this tutorial

```SQL
UPDATE Employees
SET DepartmentId = (SELECT Id FROM Departments WHERE Description = Department)
```

| **Id** | **Forename** | **Surname** | **Department** | **DepartmentId** |
| :----: | :----------: | :---------: | :------------: | :--------------: |
|   5    |   Jennifer   |    Pratt    |      Dev       |        1         |
|   6    |    Louie     |    Black    |       QA       |        2         |
|   7    |  Alexandra   |  Richards   |       QA       |        2         |
|   9    |    Amelie    |   Davison   |    Support     |        3         |

Next, now we've set these values and we will _always_ expect a value in this column, we are going to set the column to be `NOT NULL`

```SQL
ALTER TABLE Employees
ALTER COLUMN DepartmentID INT NOT NULL
```

## Drop column

Finally, in order to keep our database [normalised](https://en.wikipedia.org/wiki/Database_normalization). We are going to remove the now redundant Department column from the Employees table.

```SQL
ALTER TABLE Employees
DROP COLUMN Department
```

| **Id** | **Forename** | **Surname** | **DepartmentId** |
| :----: | :----------: | :---------: | :--------------: |
|   5    |   Jennifer   |    Pratt    |        1         |
|   6    |    Louie     |    Black    |        2         |
|   7    |  Alexandra   |  Richards   |        2         |
|   9    |    Amelie    |   Davison   |        3         |

INSERT INTO Employees ([Id], [Forename], [Surname], [Department])
SELECT 1,	'Jordan',		'Harper',		'Dev'		UNION ALL
SELECT 2,	'Toby',			'Young',		'Dev'		UNION ALL
SELECT 3,	'Alfie',		'Wong',			'Dev'		UNION ALL
SELECT 4,	'Imogen',		'Pearson',		'Dev'		UNION ALL
SELECT 5,	'Jennifer',		'Pratt',		'Dev'		UNION ALL
SELECT 6,	'Louie',		'Black',		'QA'		UNION ALL
SELECT 7,	'Alexandra',	'Richards',		'QA'		UNION ALL
SELECT 9,	'Amelie',		'Davison',		'Support'	UNION ALL
SELECT 10,	'Kyle',			'Jones',		'Support'	UNION ALL
SELECT 11,	'Sofia',		'Greenwood',	'BA'		UNION ALL
SELECT 12,	'Matthew',		'Warner',		'BA'


INSERT INTO Clients([Id], [Reference], [Description])
SELECT 1, 'NBrownGroup',	'N Brown Group'		UNION ALL
SELECT 2, 'GrahamAndBrown',	'Graham and Brown'  UNION ALL
SELECT 3, 'FarFetch',		'FarFetch'			UNION ALL
SELECT 4, 'Dyson',			'Dyson'


INSERT INTO Carriers([Id], [Reference])
SELECT 1, 'AnPost'		UNION ALL
SELECT 2, 'DHL'			UNION ALL 
SELECT 3, 'DPD'			UNION ALL 
SELECT 4, 'Hermes'		UNION ALL
SELECT 5, 'Lineten'		UNION ALL
SELECT 6, 'PostNL'		UNION ALL
SELECT 7, 'Yodel'


INSERT INTO Products([Id], [Reference])
SELECT 1, 'SortedPro'	UNION ALL 
SELECT 2, 'React'		UNION ALL
SELECT 3, 'MPD'


INSERT INTO WorkItems([Id], [ClientId], [CarrierId], [ProductId], [Reference], [Title])
SELECT 1,  1,		NULL,	2, 42154, 'React integration'				UNION ALL
SELECT 2,  2,		NULL,	1, 31548, 'SortedPRO integration'			UNION ALL
SELECT 3,  3,		3,		1, 52923, 'DPD routing data import'			UNION ALL
SELECT 4,  3,		5,		1, 53023, 'Lineten booking service'			UNION ALL
SELECT 5,  3,		5,		1, 53015, 'Lineten collection notes'		UNION ALL 
SELECT 6,  NULL,	7,		3, 12364, 'New feature for Yodel'


INSERT INTO Activities ([WorkItemId], [Date], [StartTime], [EndTime], [ActivityType], [EmployeeId])
SELECT 1, '2019-02-22', '08:30', '11:30', 'Analysis',		11	UNION ALL
SELECT 1, '2019-02-27', '09:30', '12:30', 'Development',	1	UNION ALL
SELECT 1, '2019-02-27', '13:30', '17:00', 'Development',	1	UNION ALL
SELECT 1, '2019-02-28', '09:00', '12:00', 'Development',	1	UNION ALL
SELECT 1, '2019-02-28', '13:30', '17:00', 'Development',	1	UNION ALL
SELECT 1, '2019-03-01', '09:00', '11:30', 'Development',	1	UNION ALL
SELECT 1, '2019-03-02', '10:00', '12:00', 'Testing',		6	UNION ALL
SELECT 1, '2019-03-02', '13:30', '17:00', 'Testing',		6	UNION ALL
SELECT 1, '2019-03-03', '08:00', '12:00', 'Testing',		6	UNION ALL
SELECT 1, '2019-03-02', '13:00', '14:00', 'Testing',		6	UNION ALL
	   
SELECT 2, '2019-03-11', '09:45', '12:30', 'Support',		9	UNION ALL
	   
SELECT 3, '2019-03-12', '10:00', '12:00', 'Support',		10	UNION ALL
SELECT 3, '2019-03-15', '13:00', '15:00', 'Analysis',		12	UNION ALL
SELECT 3, '2019-03-17', '09:00', '11:00', 'Development',	3	UNION ALL
SELECT 3, '2019-03-17', '13:00', '16:00', 'Development',	3	UNION ALL
SELECT 3, '2019-03-18', '10:00', '12:00', 'Development',	3	UNION ALL
SELECT 3, '2019-03-19', '10:00', '12:00', 'Testing',		7	UNION ALL
SELECT 3, '2019-03-19', '13:00', '15:00', 'Testing',		7	UNION ALL
SELECT 3, '2019-03-20', '09:00', '12:00', 'Development',	5	UNION ALL
SELECT 3, '2019-03-20', '10:00', '12:00', 'Development',	5	UNION ALL
SELECT 3, '2019-03-20', '14:00', '17:00', 'Testing',		7	UNION ALL
	   
SELECT 4, '2019-03-20', '09:00', '12:30', 'Analysis',		2	UNION ALL
SELECT 4, '2019-03-20', '09:00', '12:30', 'Analysis',		3	UNION ALL
SELECT 4, '2019-03-20', '09:00', '12:30', 'Analysis',		12	UNION ALL
SELECT 4, '2019-03-22', '13:00', '17:00', 'Development',	2	UNION ALL
SELECT 4, '2019-03-23', '09:00', '12:00', 'Development',	2	UNION ALL
SELECT 4, '2019-03-23', '13:00', '17:00', 'Development',	2	UNION ALL
SELECT 4, '2019-03-24', '09:00', '12:00', 'Development',	2	UNION ALL
SELECT 4, '2019-03-24', '13:00', '17:00', 'Development',	2	UNION ALL
SELECT 4, '2019-03-25', '09:00', '12:00', 'Development',	2	UNION ALL
SELECT 4, '2019-03-25', '13:00', '17:00', 'Development',	2	UNION ALL
SELECT 4, '2019-03-26', '09:00', '11:30', 'Development',	2	UNION ALL
SELECT 4, '2019-03-26', '14:30', '17:00', 'Testing',		7	UNION ALL
SELECT 4, '2019-03-27', '09:00', '13:00', 'Testing',		7	UNION ALL
SELECT 4, '2019-03-27', '14:30', '16:00', 'Testing',		7	UNION ALL

SELECT 5, '2019-05-14', '09:00', '12:00', 'Analysis',		2	UNION ALL
SELECT 5, '2019-05-14', '09:00', '12:00', 'Analysis',		12	UNION ALL
SELECT 5, '2019-05-16', '09:00', '12:00', 'Development',	3	UNION ALL
SELECT 5, '2019-05-16', '09:00', '12:00', 'Development',	3	UNION ALL
SELECT 5, '2019-05-17', '09:00', '12:00', 'Development',	3	UNION ALL
SELECT 5, '2019-05-17', '09:00', '12:00', 'Development',	3	UNION ALL
SELECT 5, '2019-05-18', '09:00', '12:00', 'Development',	3	UNION ALL
SELECT 5, '2019-05-19', '09:00', '12:00', 'Development',	3	UNION ALL
SELECT 5, '2019-05-19', '09:00', '12:00', 'Testing',		6	UNION ALL
SELECT 5, '2019-05-19', '09:00', '12:00', 'Testing',		6	UNION ALL
SELECT 5, '2019-05-19', '09:00', '12:00', 'Testing',		6	UNION ALL
SELECT 5, '2019-05-19', '09:00', '12:00', 'Development',	3	UNION ALL
SELECT 5, '2019-05-19', '09:00', '12:00', 'Testing',		6	UNION ALL
SELECT 5, '2019-05-19', '09:00', '12:00', 'Testing',		6	UNION ALL


SELECT 5, '2019-05-19', '09:00', '12:00', 'Development',	4	UNION ALL
SELECT 5, '2019-05-19', '13:00', '17:00', 'Development',	4	UNION ALL
SELECT 5, '2019-05-20', '09:00', '12:00', 'Development',	4	UNION ALL
SELECT 5, '2019-05-20', '13:00', '17:00', 'Development',	4	UNION ALL
SELECT 5, '2019-05-21', '09:00', '12:00', 'Development',	4	UNION ALL
SELECT 5, '2019-05-21', '13:00', '17:00', 'Development',	4	UNION ALL
SELECT 5, '2019-05-22', '09:00', '12:00', 'Development',	4	UNION ALL
SELECT 5, '2019-05-22', '13:00', '17:00', 'Development',	4	UNION ALL
SELECT 5, '2019-05-23', '10:00', '12:00', 'Testing',		6	UNION ALL
SELECT 5, '2019-05-23', '13:00', '17:00', 'Testing',		6
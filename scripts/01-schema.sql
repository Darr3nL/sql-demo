IF NOT EXISTS (SELECT 1 FROM master.dbo.sysdatabases  WHERE name = 'TimesheetDemo')
BEGIN
	CREATE DATABASE [TimesheetDemo]
END
GO

USE [TimesheetDemo]
GO

IF OBJECT_ID('Employees', 'U') IS NULL
BEGIN
	CREATE TABLE Employees (
		[Id]			INT NOT NULL PRIMARY KEY,
		[Forename]		NVARCHAR(100) NOT NULL,
		[Surname]		NVARCHAR(100) NOT NULL,
		[Department]	NVARCHAR(100) NOT NULL
	)
END

IF OBJECT_ID('Clients', 'U') IS NULL
BEGIN
	CREATE TABLE Clients (
		[Id]			INT NOT NULL PRIMARY KEY,
		[Reference]		NVARCHAR(500) NOT NULL UNIQUE,
		[Description]	NVARCHAR(500)
	)
END

IF OBJECT_ID('Products', 'U') IS NULL
BEGIN
	CREATE TABLE Products (
		[Id]			INT NOT NULL PRIMARY KEY,
		[Reference]		NVARCHAR(500) NOT NULL UNIQUE,
	)
END

IF OBJECT_ID('Carriers', 'U') IS NULL
BEGIN
	CREATE TABLE Carriers (
		[Id]			INT NOT NULL PRIMARY KEY,
		[Reference]		NVARCHAR(500) NOT NULL UNIQUE,
	)
END

IF OBJECT_ID('WorkItems', 'U') IS NULL
BEGIN
	CREATE TABLE WorkItems (
		[Id]		INT NOT NULL PRIMARY KEY,
		[Reference] INT NOT NULL UNIQUE,
		[Title]		NVARCHAR(500),
		[ClientId]	INT,
		[ProductId] INT,
		[CarrierId] INT,

		FOREIGN KEY ([ClientId]) REFERENCES Clients([Id]),
		FOREIGN KEY ([ProductId]) REFERENCES Products([Id]),
		FOREIGN KEY ([CarrierId]) REFERENCES Carriers([Id]),
	)
END

IF OBJECT_ID('Activities', 'U') IS NULL
BEGIN
	CREATE TABLE Activities (
		[Id]			INT IDENTITY(1,1) PRIMARY KEY,
		[WorkItemId]	INT NOT NULL,
		[Date]			DATETIME NOT NULL,
		[StartTime]		TIME,
		[EndTime]		TIME,
		[ActivityType]	NVARCHAR(500),
		[EmployeeId]	INT,

		FOREIGN KEY ([WorkItemId]) REFERENCES WorkItems(Id),
		FOREIGN KEY ([EmployeeId]) REFERENCES Employees(Id),
	)
END
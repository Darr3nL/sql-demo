USE [TimesheetDemo]
GO

IF OBJECT_ID('Activities', 'U') IS NOT NULL 
BEGIN
    DROP TABLE Activities
END

IF OBJECT_ID('WorkItems', 'U') IS NOT NULL 
BEGIN
    DROP TABLE WorkItems
END

IF OBJECT_ID('Carriers', 'U') IS NOT NULL 
BEGIN
	DROP TABLE Carriers
END

IF OBJECT_ID('Products', 'U') IS NOT NULL 
BEGIN
    DROP TABLE Products
END 

IF OBJECT_ID('Clients', 'U') IS NOT NULL 
BEGIN
    DROP TABLE Clients
END

IF OBJECT_ID('Departments', 'U') IS NOT NULL 
BEGIN
	DROP TABLE Departments
END

IF OBJECT_ID('Employees', 'U') IS NOT NULL 
BEGIN
	DROP TABLE Employees
END